<?php include('php/header.php'); ?>

<?php
    // Récupération de tous les articles (nom extrait, image, date)
    $requete2='SELECT id_article, nom_article, extrait_article, image_article, date_article
        FROM article
        ORDER by date_article DESC';
    $resultats=$bdd->query($requete2);
    $actualites=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();
?>

<header class="header-small" id="actu">
    <?php include('php/nav.php'); ?>
    <img src="images/header2.jpg" alt="Innovatherm">
</header>

<div class="annuaire_entete">
    <p class="filariane">
        <a href="index.php">Accueil</a>
        <span class="slash"> / </span>
        <span>Actualités</span>
    </p>

    <p class="annuaire_subtitle">Cluster d'excellence</p>
    <h1>Actualités</h1>
</div>

<div class="actu page_actu marge_bot" id="actu-list">
    <div class="actu_container">
        <?php foreach( $actualites as $actualite ): ?>
            <?php include('php/articles_actu.php'); ?>
        <?php endforeach; ?>
    </div>
</div>

<?php include('php/footer.php'); ?>
