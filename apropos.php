<?php include('php/header.php'); ?>
<?php
// Récupération des 3 dernières actualités (nom extrait, image, date)
$requete2='SELECT id_article, nom_article, extrait_article, image_article, date_article
    FROM article
    ORDER by date_article DESC LIMIT 3';
$resultats=$bdd->query($requete2);
$actualites=$resultats->fetchAll(PDO::FETCH_OBJ);
$resultats->closeCursor();

?>

<header class="header-small" id="annuaire">
    <?php include('php/nav.php'); ?>
    <img src="images/header2.jpg" alt="Innovatherm">
</header>

<div class="annuaire_entete">
    <p class="filariane">
        <a href="index.php">Accueil</a>
        <span class="slash"> / </span>
        <span>A Propos</span>
    </p>

    <p class="annuaire_subtitle">Cluster d'excellence</p>
    <h1>A propos</h1>
</div>


<section class="page_apropos">
    <div class="contact_section apropos_scroll_1">
        <h2>Catalyseur de synergies</h2>
        <p class="apropos_excerpt">
            Véritable catalyseur de synergies au sein des stations thermales d’Auvergne-Rhône-Alpes, le Cluster d’Excellence Innovatherm a pour principal objet d’améliorer la compétitivité des entreprises thermales et de les positionner comme des pôles d’excellence de la prévention santé via l’accompagnement de projets collaboratifs innovants.
        </p>
        <p>
            Face au désir des populations de vivre le plus longtemps possible en bonne santé, les stations thermales offrent en effet d’exceptionnelles potentialités pour devenir des sites d’excellence de la prévention santé, à savoir des lieux où l’on apprend à rester en bonne santé le plus longtemps possible en mettant en œuvre la prévention primaire (apprendre à modifier son mode de vie pour prévenir une pathologie), secondaire (apprendre à mieux vivre avec une maladie chronique) ou tertiaire (réhabilitation post-maladie).
        </p>
    </div>

    <div class="contact_section apropos_scroll_2">
        <h2>Favoriser les interconnexions</h2>
        <p class="apropos_excerpt">
            Outre le développement de l’innovation dans le secteur du thermalisme et de la valorisation des eaux thermales, le Cluster favorise l’interconnexion entre les entreprises du territoire, aide à conquérir de nouveaux marchés via le lancement de produits inédits, et accompagne globalement la performance de l’entreprise.
        </p>
        <p>
            Positionné sur la filière thermale, à l’interface de la santé et du tourisme, le Cluster a ainsi mis en place une dynamique contribuant à construire les stations thermales de demain et joue en cela le rôle d’un véritable pôle structurant de l’aménagement du territoire, en phase avec les valeurs de naturalité et d’exigence chères à la région Auvergne-Rhône-Alpes.
        </p>
    </div>

    <div class="contact_section apropos_scroll_3">
        <h2>Mise en relation</h2>
        <p class="apropos_excerpt">
            Réunissant, en sa qualité de cluster, des membres de profils très divers, le Cluster d’Excellence Innovatherm facilite l’interconnexion et la mise en relation entre les porteurs de projets, les entreprises et les centres de recherche.
        </p>
        <p>
            Selon le type d’action, le Cluster peut également apporter une aide technique d’ingénierie ou encore aider, par le biais d’une communication adaptée, à la valorisation de ladite recherche auprès de différents publics.
        </p>
        <p class="devenirmembre"><a class="button button_blue" href="list.php">Voir les membres</a></p>

    </div>

    <div class="contact_section apropos_scroll_4">
        <h2>Ouvert à tous</h2>
        <p>
            Comportant tout aussi bien des entreprises privées, des laboratoires de recherche et des institutions, le Cluster d’Excellence Innovatherm est un groupement ouvert à toutes les structures (TPE, PME, centres de recherches, universités, associations…) souhaitant porter ou participer à un projet en lien avec le thermalisme, l’eau thermale ou le développement économique des stations thermales au sens large.
        </p>
        <p class="devenirmembre"><a class="button button_blue" href="contact.php#inscription">Devenir membre</a></p>
    </div>
</section>

<section class="actu marge_bot">
    <h2>A la une d'Innovatherm</h2>
    <div class="actu_container">
        <?php foreach( $actualites as $actualite ): ?>
            <?php include('php/articles_actu.php'); ?>
        <?php endforeach; ?>
    </div>
    <p class="memberlist"><a class="button button_purple actu_button" href="actu.php">Voir tous l'actualité</a></p>
    
</section>



<?php include('php/footer.php'); ?>
