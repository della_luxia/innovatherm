<?php include('php/header.php'); ?>

<?php

// Récupération des données de l'article
$requete1='SELECT *
    FROM article
    WHERE article.id_article = '.$_GET['id'];
$resultats=$bdd->query($requete1);
$article=$resultats->fetch(PDO::FETCH_OBJ);
$resultats->closeCursor();


?>



<header class="header-small" id="annuaire">
    <?php include('php/nav.php'); ?>
    <img src="images/header2.jpg" alt="Innovatherm">
</header>

<div class="annuaire_entete">
    <p class="filariane">
        <a href="index.php">Accueil</a>
        <span class="slash"> / </span>
        <a href="actu.php">Actualités</a>
        <span class="slash"> / </span>
        <span><?= $article->nom_article; ?></span>
    </p>

    <p class="annuaire_subtitle">Cluster d'excellence</p>
    <h1><?= $article->nom_article; ?></h1>
</div>

<section class="article">

    <img src="images/<?= $article->image_article; ?>" alt="<?= $article->nom_article; ?>">
    <div class="article_content">
        <p class="article_excerpt">
            <?= $article->extrait_article; ?>
        </p>
        <p>
            <?= $article->texte_article; ?>
        </p>
    </div>


</section>


















<?php include('php/footer.php'); ?>
