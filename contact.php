<?php include('php/header.php'); ?>

<header class="header-small" id="contact">
    <?php include('php/nav.php'); ?>
    <img src="images/header2.jpg" alt="Innovatherm">
</header>

<div class="annuaire_entete">
    <p class="filariane">
        <a href="index.php">Accueil</a>
        <span class="slash"> / </span>
        <span>Contact</span>
    </p>

    <p class="annuaire_subtitle">Cluster d'excellence</p>
    <h1>Contact</h1>
</div>

<h2 class="nouscontacter">Nous contacter ?</h2>
<section class="" id="contact_form">

    <div class="inscription_form ">
        <form method="post">
            <div class="form_group">
                <label for="nom">Nom</label>
                <input class="form_input" type="text" name="nom" value="">
            </div>
            <div class="form_group">
                <label for="prenom">Prénom</label>
                <input class="form_input" type="text" name="prenom" value="">
            </div>

            <div class="form_group">
                <label for="mail">E-mail</label>
                <input class="form_input" type="mail" name="mail" value="">
            </div>
            <div class="form_group">
                <label for="adresse">Adresse</label>
                <input class="form_input" type="mail" name="adresse" value="">
            </div>

            <div class="form_area">
                <label for="motivation">Message</label>
                <textarea class="form_input" name="motivation"></textarea>
            </div>

            <input class="button button_purple modal_submit" type="submit" name="envoyer" value="Envoyer">
        </form>
    </div>
</section>

<section class="section" id="contact_inscription">
    <div class="inscription_infos">
        <h2>Devenir membre ?</h2>
        <p>
            Le premier intérêt pour une structure de devenir membre du <strong>Cluster d’Excellence</strong>. Innovatherm est d’entrer de plain-pied dans la démarche de <strong>projet collaboratif</strong> innovant en lien dans l’univers du thermalisme au sens large. Non seulement le nouveau membre entre en contact direct avec les autres membres de l’association mais il bénéficie, auprès des instances partenaires, de la légitimité apportée par le Cluster.
        </p>
        <p>
            Les porteurs de projet mais également les structures innovantes susceptibles d’apporter leur compétence spécifique dans le cadre d’une étude et les structures <strong>en quête de nouveaux débouchés</strong> ont à ce titre tout intérêt à adhérer au Cluster d’Excellence Innovatherm.
        </p>
    </div>


    <div class="inscription_form ">
        <form method="post">
            <div class="form_group">
                <label for="nom">Nom</label>
                <input class="form_input" type="text" name="nom" value="" required>
            </div>

            <div class="form_group">
                <label for="mail">E-mail</label>
                <input class="form_input" type="mail" name="mail" value="" required>
            </div>

            <div class="form_area">
                <label for="motivation">Motivation</label>
                <textarea class="form_input" name="motivation" required></textarea>
            </div>

            <input class="button button_purple modal_submit" type="submit" name="envoyer" value="Envoyer">
        </form>
    </div>


</section>

<section class="home_contact">

    <div class="home_contact_img"></div>

    <div class="home_contact_infos">
        <h2>Nous contacter</h2>
        <div class="home_contact_infos_container">
            <div class="home_contact_infos_adress">
                <i class="fas fa-map-marker-alt"></i>
                <p>8 allée Evariste Galois <br>63000 Clermont-Ferrand</p>
            </div>
            <div class="home_contact_infos_content">
                <div class="home_contact_infos_tel">
                    <i class="fas fa-phone-alt"></i>
                    <p>+33 4 73 44 56 09</p>
                </div>
                <div class="home_contact_infos_mail">
                    <i class="fas fa-paper-plane"></i>
                    <p>ac.fournier@innovatherm.fr</p>
                </div>
            </div>
        </div>
    </div>

    <div class="home_contact_map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2783.570334870631!2d3.1287697157741237!3d45.75975912169339!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f71c4445e98805%3A0xba55d1834a86fa28!2s8%20All%C3%A9e%20Evariste%20Galois%2C%2063000%20Clermont-Ferrand!5e0!3m2!1sfr!2sfr!4v1586169985509!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>

</section>






<?php include('php/footer.php'); ?>
