<?php include('php/header.php'); ?>

<?php
    // Récupération de 8 établissements (nom, lieu, image, spécialités)
    $requete1='SELECT etablissement.id_etablissement, etablissement.nom_etablissement, etablissement.lieu_etablissement, etablissement.image_etablissement, GROUP_CONCAT(specialite.abreviation_specialite) AS liste_specialite
        FROM etablissement, specialite, etablissement_specialite
        WHERE etablissement.id_etablissement = etablissement_specialite.id_etablissement AND specialite.id_specialite = etablissement_specialite.id_specialite
        GROUP BY etablissement.nom_etablissement
        ORDER BY RAND() LIMIT 8';
    $resultats=$bdd->query($requete1);
    $etablissements=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();

    // Récupération des 3 dernières actualités (nom extrait, image, date)
    $requete2='SELECT id_article, nom_article, extrait_article, image_article, date_article
        FROM article
        ORDER by date_article DESC LIMIT 3';
    $resultats=$bdd->query($requete2);
    $actualites=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();
?>

<header class="home">
    <div class="header_paralaxe">
        <img src="images/fond.png" alt="Innovatherm">
        <img src="images/femme.png" alt="Innovatherm">

        <p>
            <span>I</span>
            <span>N</span>
            <span>N</span>
            <span>O</span>
            <span>V</span>
            <span>A</span>
            <span>T</span>
            <span>H</span>
            <span>E</span>
            <span>R</span>
            <span>M</span>
        </p>
    </div>


    <?php include('php/nav.php'); ?>
    <div class="logo"><img src="images/logo1.svg" alt="Innovatherm_logo"> </div>
    <div class="scroller"></div>
</header>

<section class="section" id="apropos">
    <p class="surtitre">Cluster d'excellence</p>
    <h1>Auvergne - Rhône - Alpes - Thermal</h1>
    <p class="text">
        Se regrouper, pour mieux innover, et gagner en compétitivité autour des entreprises thermales d'Auvergne-Rhône-Alpes
    </p>
    <a class="button button_purple" href="apropos.php">Le Cluster</a>
    <a class="button button_blue" href="membres.php">Les membres</a>
</section>

<section id="etablissements">

    <div class="etablissements">

        <?php foreach ($etablissements as $etablissement): ?>

            <a href="single.php?id=<?= $etablissement->id_etablissement; ?>" class="etablissement">
                <img src="images/<?= $etablissement->image_etablissement; ?>" alt="<?= $etablissement->nom_etablissement; ?>">
                <h2><?= $etablissement->nom_etablissement; ?></h2>
                <p class="located">
                    <i class="fas fa-map-marker-alt"></i>
                    <span><?= $etablissement->lieu_etablissement; ?></span>
                </p>
                <?php $specialites = explode(',', $etablissement->liste_specialite); ?>
                <p class="specialization">
                    <?php
                        foreach ( $specialites as $key => $specialite )
                        {
                            if( ($key + 1) == count($specialites) ) { echo $specialite; }
                            else { echo $specialite." - "; }
                        }
                    ?>
                </p>

                <p class="ensavoirplus">En savoir plus</p>
            </a>

        <?php endforeach; ?>

    </div>

    <p class="memberlist"><a class="button button_purple" href="list.php">Voir tous les membres</a></p>

</section>

<section class="section" id="actu">
    <div class="actu">
        <h2>A la une d'Innovatherm</h2>

        <div class="actu_container">
            <?php foreach( $actualites as $actualite ): ?>
                <?php include('php/articles_actu.php'); ?>
            <?php endforeach; ?>
        </div>

        <p class="memberlist"><a class="button button_purple actu_button" href="actu.php">Voir tous l'actualité</a></p>

    </div>
</section>



<section class="section" id="inscription">
    <div class="inscription_infos">
        <h2>Devenir membre ?</h2>
        <p>
            Le premier intérêt pour une structure de devenir membre du <strong>Cluster d’Excellence</strong>. Innovatherm est d’entrer de plain-pied dans la démarche de <strong>projet collaboratif</strong> innovant en lien dans l’univers du thermalisme au sens large. Non seulement le nouveau membre entre en contact direct avec les autres membres de l’association mais il bénéficie, auprès des instances partenaires, de la légitimité apportée par le Cluster.
        </p>
        <p>
            Les porteurs de projet mais également les structures innovantes susceptibles d’apporter leur compétence spécifique dans le cadre d’une étude et les structures <strong>en quête de nouveaux débouchés</strong> ont à ce titre tout intérêt à adhérer au Cluster d’Excellence Innovatherm.
        </p>
    </div>
    <div class="inscription_form ">
        <form method="post">
            <div class="form_group">
                <label for="nom">Nom</label>
                <input class="form_input" type="text" name="nom" value="">
            </div>

            <div class="form_group">
                <label for="mail">E-mail</label>
                <input class="form_input" type="mail" name="mail" value="">
            </div>

            <div class="form_area">
                <label for="motivation">Motivation</label>
                <textarea class="form_input" name="motivation"></textarea>
            </div>

            <input class="button button_purple" type="submit" name="envoyer" value="Envoyer">
        </form>
    </div>
</section>

<section class="home_contact">

    <div class="home_contact_img"></div>

    <div class="home_contact_infos">
        <h2>Nous contacter</h2>
        <div class="home_contact_infos_container">
            <div class="home_contact_infos_adress">
                <i class="fas fa-map-marker-alt"></i>
                <p>8 allée Evariste Galois <br>63000 Clermont-Ferrand</p>
            </div>
            <div class="home_contact_infos_content">
                <div class="home_contact_infos_tel">
                    <i class="fas fa-phone-alt"></i>
                    <p>+33 4 73 44 56 09</p>
                </div>
                <div class="home_contact_infos_mail">
                    <i class="fas fa-paper-plane"></i>
                    <p>ac.fournier@innovatherm.fr</p>
                </div>
            </div>
        </div>
    </div>

    <div class="home_contact_map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2783.570334870631!2d3.1287697157741237!3d45.75975912169339!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f71c4445e98805%3A0xba55d1834a86fa28!2s8%20All%C3%A9e%20Evariste%20Galois%2C%2063000%20Clermont-Ferrand!5e0!3m2!1sfr!2sfr!4v1586169985509!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>

</section>



<?php include('php/footer.php'); ?>
