$(document).ready(function() {
    /* --- variables --- */
    // scroll
    scrollTop = $(window).scrollTop();
    scrollPrev = scrollTop;

    // elements
    windowHeight = $(window).height();
    bodyHeight = $('body').height();

    footerHeight = $('footer').height() + 200;
    copyRightHeight = $('.copyright').height() + 20;

    /* ------------------------ */
    /* --- Toutes les pages --- */
    /* ------------------------ */

    $(window).on('scroll', function(){
        scrollTop = $(window).scrollTop();

        scroll_appear(scrollTop, '#apropos', 'active');
        scroll_appear(scrollTop, '#actu .actu', 'active');
        scroll_appear(scrollTop, '#inscription', 'active');
        scroll_appear(scrollTop, '.apropos_scroll_1', 'active');
        scroll_appear(scrollTop, '.apropos_scroll_2', 'active');
        scroll_appear(scrollTop, '.apropos_scroll_3', 'active');
        scroll_appear(scrollTop, '.apropos_scroll_4', 'active');
        scroll_appear(scrollTop, '#contact_inscription', 'active');
        scroll_appear(scrollTop, '#contact_form', 'active');
        scroll_appear(scrollTop, '.nouscontacter', 'active');
    });

    /* --- Burger menu --- */
    $('.burger').on('click', function()
    {
        if (window.matchMedia("(max-width: 900px)").matches)
        {
            $('.burger_ligne').toggleClass('active');
            $('.modal-nav').toggleClass('active');
            $('.rs').toggleClass('active');
            $('.barre').toggleClass('active');
        }
        if (window.matchMedia("(min-width: 901px)").matches)
        {
            $('.nav').toggleClass('active');
            $('.rs').toggleClass('active');
            $('.burger_ligne').toggleClass('active');
        }
    });

    /* ---Back To Top--- */
    $('.backtotop').on('click', function()
    {
        $("html, body").animate({ scrollTop: "0" });
    });
    $(document).on('scroll', function()
    {
        scrollTop = $(window).scrollTop();

        if( scrollTop < scrollPrev - 100 )
        {
            // on monte
            if( scrollTop > 1000 )
            {
                $('.backtotop').addClass('visible');
            }
            else
            {
                $('.backtotop').removeClass('visible');
            }
            scrollPrev = scrollTop; // on actualise la variable
        }

        else if( scrollTop > scrollPrev + 100 )
        {
            // on descend
            $('.backtotop').removeClass('visible');
            scrollPrev = scrollTop; // on actualise la variable
        }
    });

    /* ---Form label--- */
    $('.form_input').on('input', function()
    {
        if( $(this).val() == "" )
        {
            $(this).removeClass('active'); // textarea
            $(this).prev('label').removeClass('selected');
        }
        else
        {
            $(this).prev('label').addClass('selected');
        }

        // Hauteur du textarea
        if( $(this).attr('name') == "motivation" && $(this).val() != "" )
        {
            $(this).addClass('active');
        }
    });

    /* ---Modal message envoyé--- */
    $('input[type="submit"]').on('click', function(event)
    {
        event.preventDefault();
        $(".modal_envoyer").addClass('active');

    });

    $(document).on('scroll', function(){

        if( $(".modal_envoyer").hasClass('active') )
        {
            $(".modal_envoyer").removeClass('active');
        }

    });


    $('.modal_envoyer').click(function(){

        $(".modal_envoyer").removeClass('active');

    });



    /* ----------------- */
    /* --- Index.php --- */
    /* ----------------- */

    /* --- header --- */
    $('.header_paralaxe p span').addClass('active');

    /* --- header logo (responsive) --- */
    // desktop
    if (window.matchMedia("(min-width: 901px)").matches)
    {
        $('header.home .logo img').attr('src', 'images/logo1.svg');
    }
    // tablet
    if (window.matchMedia("(max-width: 900px)").matches)
    {
        $('header.home .logo img').attr('src', 'images/logo2.svg');
    }

    /* ---------------- */
    /* --- list.php --- */
    /* ---------------- */

    /* --- Filtres tablet/mobile --- */
    $('.filter-small').on('click', function()
    {
        $('.modal-filtre').addClass('active');
    });
    $('.modal-filtre .filter li, .modal-filtre .croix').on('click', function() {
         $('.modal-filtre').removeClass('active');
    });

    /* --- système de filtres --- */
    $('.filter li').click(function() {
        // applique le filtre
        $('.filter li').removeClass('active');
        $(this).addClass('active');
        var filtreactif = $(this).attr("filtre");

        if ( filtreactif == 'tout' )
        {
            $( '.annuaire_etablissement' ).addClass('visible');
        }
        else
        {
            $( '.annuaire_etablissement' ).removeClass('visible');
            $( '.annuaire_etablissement.' + filtreactif ).addClass('visible');
        }

        // permute le nom du filtre sélectionné
        var nom = $(this).attr('nom');
        var abreviation = $(this).attr('abreviation');
        $.each( $('.filter li'), function(index, item) {
            var nom = $(item).attr('nom');
            var abreviation = $(item).attr('abreviation');
            $( item ).text(abreviation);
        });
        $(this).text(nom);
    });

    /* --- filtrage par l'url --- */
    var filtrelink = $('.filter').attr('data-filtre');
    if( filtrelink != "" )
    {
        $('.filter li').removeClass('active');
        $.each( $('.filter li'), function(index, item) {
            if( $(item).attr('filtre') == filtrelink )
            {
                var nom = $(item).attr('nom');
                $( item ).text(nom);
                $( item ).addClass('active');
            }
        });
        $( '.annuaire_etablissement' ).removeClass('visible');
        $( '.annuaire_etablissement.' + filtrelink ).addClass('visible');
    }

    /* ------------------ */
    /* --- single.php --- */
    /* ------------------ */

    /* --- Galerie (responsive) --- */
    // desktop
    if (window.matchMedia("(min-width: 901px)").matches)
    {
        $('.single_galery img').on('click', function()
        {
            var src = $(this).attr('src');
            $('.single_img img').attr('src', src);
            $('.img_galerie').removeClass('visible');
            $(this).parent('.img_galerie').addClass('visible');
        });
    }
    // tablet
    if (window.matchMedia("(max-width: 900px)").matches) {
        $('.single_galery').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            arrows: false,
            dots: true,
            customPaging: function(slider, i) {
              return '<div class="slick-dot" id=' + i + "></div>";
            },
        });
    }

    /* --- Animations --- */
    if (window.matchMedia("(min-width: 901px)").matches)
    {
        // en savoir plus scroll
        $('.single_ensavoirplus').on('click', function(event)
        {
            event.preventDefault();
            $("html, body").animate({ scrollTop: windowHeight });
        });

        // Init - image de droite
        scrollTest('all', scrollTop, 150, 'add-sup', '.single_img', 'active');
        // Init image et galerie par rapport au footer
        var footerTop = bodyHeight - footerHeight - scrollTop - windowHeight - copyRightHeight;
        scrollTest( 'all', footerTop, 0, 'add-inf', '.single_img', 'blocked' );
        scrollTest( 'all', footerTop, 0, 'add-inf', '.single_galery', 'blocked' );
        $(window).on('scroll', function() {
            scrollTop = $(window).scrollTop();

            // image de droite
            scrollTest('all', scrollTop, 150, 'add-sup', '.single_img', 'active');

            // image et galerie par rapport au footer
            footerTop = bodyHeight - footerHeight - scrollTop - windowHeight - copyRightHeight;
            scrollTest( 'all', footerTop, 0, 'add-inf', '.single_img', 'blocked' );
            scrollTest( 'all', footerTop, 0, 'add-inf', '.single_galery', 'blocked' );
        });
    }
});

function scrollTest(comparetype, compare1, compare2, action, selector, attr_class)
{
    switch(comparetype)
    {
        case 'all':
            switch( action )
            {
                case 'add-inf':
                    scrollTest('inf', compare1, compare2, 'add', selector, attr_class);
                    scrollTest('sup', compare1, compare2, 'remove', selector, attr_class);
                break;
                case 'add-sup':
                    scrollTest('sup', compare1, compare2, 'add', selector, attr_class);
                    scrollTest('inf', compare1, compare2, 'remove', selector, attr_class);
                break;
                case 'remove-inf':
                    scrollTest('inf', compare1, compare2, 'remove', selector, attr_class);
                    scrollTest('sup', compare1, compare2, 'add', selector, attr_class);
                break;
                case 'remove-sup':
                    scrollTest('sup', compare1, compare2, 'remove', selector, attr_class);
                    scrollTest('inf', compare1, compare2, 'add', selector, attr_class);
                break;
            }
        break;
        case 'inf':
            if( compare1 <= compare2 )
            {
                switch( action )
                {
                    case 'add':
                        $(selector).addClass(attr_class);
                    break;
                    case 'remove':
                        $(selector).removeClass(attr_class);
                    break;
                    case 'toggle':
                        $(selector).toggleClass(attr_class);
                    break;
                }
            }
        break;
        case 'sup':
            if( compare1 > compare2)
            {
                switch( action )
                {
                    case 'add':
                        $(selector).addClass(attr_class);
                    break;
                    case 'remove':
                        $(selector).removeClass(attr_class);
                    break;
                    case 'toggle':
                        $(selector).toggleClass(attr_class);
                    break;
                }
            }
        break;
    }
}

function scroll_appear(scrollTop, select, attribut)
{
    if( $(select).length != 0 )
    {
        var posTop = $(select).offset().top;
        if (scrollTop > posTop - windowHeight) {
            $(select).addClass(attribut);
        }
    }
}
