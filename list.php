<?php include('php/header.php'); ?>

<?php
    // Récupération de la liste des spécialités (id, nom, abréviation)
    // ayant au moins une association avec un établissement
    $requete1='SELECT DISTINCT specialite.id_specialite, specialite.nom_specialite, specialite.abreviation_specialite
        FROM specialite, etablissement, etablissement_specialite
        WHERE etablissement.id_etablissement = etablissement_specialite.id_etablissement
        AND specialite.id_specialite = etablissement_specialite.id_specialite';
    $resultats=$bdd->query($requete1);
    $filtres=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();

    // Récupération des établissements et spécialités (id, nom, lieu, image, spécialités)
    $requete2='SELECT etablissement.id_etablissement, etablissement.nom_etablissement, etablissement.lieu_etablissement, etablissement.image_etablissement, GROUP_CONCAT(specialite.abreviation_specialite) AS liste_specialite
        FROM etablissement, specialite, etablissement_specialite
        WHERE etablissement.id_etablissement = etablissement_specialite.id_etablissement
        AND specialite.id_specialite = etablissement_specialite.id_specialite
        GROUP BY etablissement.nom_etablissement;';
    $resultats=$bdd->query($requete2);
    $etablissements=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();

    // Récupération des 3 dernières actualités (nom extrait, image, date)
    $requete3='SELECT id_article, nom_article, extrait_article, image_article, date_article
        FROM article
        ORDER by date_article DESC LIMIT 3';
    $resultats=$bdd->query($requete3);
    $actualites=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();
?>

<header class="header-small" id="annuaire">
    <?php include('php/nav.php'); ?>
    <img src="images/header2.jpg" alt="Innovatherm">
</header>

<div class="annuaire_entete">
    <p class="filariane">
        <a href="index.php">Accueil</a>
        <span class="slash"> / </span>
        <span>Membres</span>
    </p>
    <p class="annuaire_subtitle">Cluster d'excellence</p>
    <h1>Membres</h1>
</div>

<section class="section_list">

    <ul class="filter" data-filtre="<?php if( isset($_GET['filtre']) && !empty($_GET['filtre']) ) { echo $_GET['filtre']; }
    ?>">
        <li class="active" filtre="tout">Tout</li>
        <?php foreach($filtres as $filtre): ?>
            <li abreviation="<?= $filtre->abreviation_specialite; ?>"
                nom="<?= $filtre->nom_specialite; ?>"
                filtre="<?= strtolower($filtre->abreviation_specialite); ?>">
                <?= $filtre->abreviation_specialite; ?>
            </li>
        <?php endforeach; ?>
    </ul>

    <p class="filter-small"> <span class="button button_purple">Filtres</span> </p>

    <div class="annuaire">
        <?php foreach( $etablissements as $etablissement ): ?>

            <?php $spe = str_replace(',', ' ', strtolower($etablissement->liste_specialite)); ?>
            <a href="single.php?id=<?= $etablissement->id_etablissement; ?>" class="visible annuaire_etablissement <?= $spe; ?>" filtre="gyn">
                <img src="images/<?= $etablissement->image_etablissement; ?>" alt="<?= $etablissement->nom_etablissement; ?>">
                <h2><?= $etablissement->nom_etablissement; ?></h2>
                <ul>
                    <?php $specialites = explode(',', $etablissement->liste_specialite); ?>
                    <?php foreach($specialites as $specialite): ?>
                        <li><?= $specialite; ?></li>
                    <?php endforeach; ?>
                </ul>
            </a>

        <?php endforeach; ?>
    </div>

</section>

<section class="section actu">
    <h2>Vous serez aussi intéressé</h2>
    <div class="actu page_actu">
        <div class="actu_container">
            <?php foreach( $actualites as $actualite ): ?>
                <?php include('php/articles_actu.php'); ?>
            <?php endforeach; ?>
        </div>

        <p class="memberlist"><a class="button button_purple actu_button" href="actu.php">Voir tous l'actualité</a></p>
    </div>
</section>

<?php include('php/footer.php'); ?>
