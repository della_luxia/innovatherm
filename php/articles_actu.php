<!-- <div class="actu_container"> -->
    <a href="article.php?id=<?= $actualite->id_article; ?>" class="actu_article">
        <div class="article_img_container"><img src="images/<?= $actualite->image_article; ?>" alt="<?= $actualite->nom_article; ?>"></div>

        <?php $date = explode('-', $actualite->date_article); ?>
        <p class="actu_date">
            <span><?= $date[2]; ?></span>
            <span><?= textLimit(dateMonth($date[1]), 3, false); ?></span>
            <span><?= $date[0]; ?></span>
        </p>

        <div class="actu_content">
            <h3><?= $actualite->nom_article; ?></h3>
            <p class="actu_excerpt">
                <?php textLimit($actualite->extrait_article, 120); ?>
            </p>
            <p class="actu_ensavoirplus">En savoir plus</p>
        </div>
    </a>



<!-- </div> -->
