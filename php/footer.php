        <footer>
            <div class="footer">
                <div class="footer_contact">
                    <div>
                        <div class="logo"><img src="images/logo2.svg" alt="Innovatherm_logo"> </div>
                        <p>8 allée Evariste Galois</p>
                        <p>63000 Clermont-Ferrand</p>
                        <p>+33 4 73 44 56 09</p>
                    </div>
                </div>

                <div class="footer_nav">
                    <div>
                        <a href="index.php">Accueil</a>
                        <a href="apropos.php">A propos</a>
                        <a href="actu.php">Actualités</a>
                        <a href="list.php">Membres</a>
                        <a href="contact.php">Contact</a>
                    </div>
                </div>

                <div class="footer_social">
                    <div>
                        <p>Nous suivre</p>
                        <div class="footer_social_rs">
                            <a target="_blank" href="https://www.facebook.com/amandiine.debas"><i class="fab fa-facebook-f"></i></a>
                            <a target="_blank" href="https://www.instagram.com/della_luxia/"><i class="fab fa-instagram"></i></a>
                            <a target="_blank" href="https://twitter.com/explore"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </footer>
        <div class="copyright">
            <p>Design & Développement par <span><a href="http://amandine-debas.fr/">Amandine Debas</a></span></p>
        </div>

        <div class="backtotop active">
            <i class="fas fa-arrow-up"></i>
        </div>

        <div class="backtotop">
            <div class="active"></div>
            <i class="fas fa-arrow-up"></i>
        </div>

        <div class="modal modal-nav">

            <ul class="nav">
                <li><a href="index.php">Accueil</a></li>
                <li><a href="apropos.php">A propos</a></li>
                <li><a href="actu.php">Actualités</a></li>
                <li><a href="list.php">Membres</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>

        </div>

        <div class="modal modal-filtre">

            <div class="croix">
                <div class="croix-barre"></div>
                <div class="croix-barre"></div>
            </div>

            <ul class="filter" data-filtre="<?php if( isset($_GET['filtre']) && !empty($_GET['filtre']) ) { echo $_GET['filtre']; } ?>">
                <li class="active" filtre="tout">Tout</li>
                <?php foreach($filtres as $filtre): ?>
                    <li abreviation="<?= $filtre->abreviation_specialite; ?>"
                        nom="<?= $filtre->nom_specialite; ?>"
                        filtre="<?= strtolower($filtre->abreviation_specialite); ?>">
                        <?= $filtre->abreviation_specialite; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <!-- modal bouton envoyer -->
        <div class="modal_envoyer">
                <div class="modal_text">
                <p>Message envoyé ! :)</p>
            </div>
        </div>


        <!-- Js -->
        <script src="js/script.js" type="text/javascript"></script>
    </body>
</html>
