<?php

    function textLimit($text, $taille, $suite = true)
    {
        /*
        *    Affiche $taille caractères d'une chaine $text
        */
        if( strlen($text) < $taille )
        {
            echo $text;
        }
        else
        {
            for( $i = 0 ; $i < $taille ; $i++ )
            {
                echo $text[$i];
            }
            if($suite)
            {
                echo ' [...]';
            }
        }
    }

    function dateMonth($mois)
    {
        switch($mois)
        {
            case "01": return 'janvier'; break;
            case "02": return 'février'; break;
            case "03": return 'mars'; break;
            case "04": return 'avril'; break;
            case "05": return 'mai'; break;
            case "06": return 'juin'; break;
            case "07": return 'juillet'; break;
            case "08": return 'août'; break;
            case "09": return 'septembre'; break;
            case "10": return 'octobre'; break;
            case "11": return 'novembre'; break;
            case "12": return 'décembre'; break;
        }
    }

?>
