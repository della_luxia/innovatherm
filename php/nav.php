<div class="barres">
    <div class="barre barre_1"></div>
    <div class="barre barre_2"></div>
</div>

<nav>
    <div class="burger">
        <div class="burger_ligne"></div>
        <div class="burger_ligne"></div>
        <div class="burger_ligne"></div>
    </div>

    <ul class="nav">
        <li><a href="index.php">Accueil</a></li>
        <li><a href="apropos.php">A propos</a></li>
        <li><a href="actu.php">Actualités</a></li>
        <li><a href="list.php">Membres</a></li>
        <li><a href="contact.php">Contact</a></li>
    </ul>

    <ul class="rs">
        <li><a target="_blank" href="https://www.facebook.com/amandiine.debas"><i class="fab fa-facebook-f"></i></a></li>
        <li><a target="_blank" href="https://www.instagram.com/della_luxia/"><i class="fab fa-instagram"></i></a></li>
        <li><a target="_blank" href="https://twitter.com/explore"><i class="fab fa-twitter"></i></a></li>
    </ul>
</nav>
