<?php include('php/header.php'); ?>
<?php
    // Récupération d'un établissement (id, nom, département, lieu, site, extrait, texte)
    $requete1='SELECT *
        FROM etablissement
        WHERE etablissement.id_etablissement = '.$_GET['id'];
    $resultats=$bdd->query($requete1);
    $etablissement=$resultats->fetch(PDO::FETCH_OBJ);
    $resultats->closeCursor();

    // Récupération des réseaux sociaux de l'établissement
    $requete2='SELECT reseau.nom_reseau, reseau.lien_reseau, reseau.icon_reseau
        FROM etablissement, reseau, etablissement_reseau
        WHERE etablissement.id_etablissement = etablissement_reseau.id_etablissement
        AND reseau.id_reseau = etablissement_reseau.id_reseau
        AND etablissement.id_etablissement = '.$_GET['id'];
    $resultats=$bdd->query($requete2);
    $reseaux=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();

    // Récupération images associées
    $requete3='SELECT *
        FROM image, etablissement, etablissement_image
        WHERE image.id_image = etablissement_image.id_image
        AND etablissement.id_etablissement = etablissement_image.id_etablissement
        AND etablissement.id_etablissement = '.$_GET['id'];
    $resultats=$bdd->query($requete3);
    $images=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();

    // Récupération spécialitées associées
    $requete4='SELECT specialite.nom_specialite, specialite.abreviation_specialite
        FROM specialite, etablissement, etablissement_specialite
        WHERE etablissement.id_etablissement = etablissement_specialite.id_etablissement
        AND specialite.id_specialite = etablissement_specialite.id_specialite
        AND etablissement.id_etablissement = '.$_GET['id'];
    $resultats=$bdd->query($requete4);
    $specialites=$resultats->fetchAll(PDO::FETCH_OBJ);
    $resultats->closeCursor();

    // Récupération nombre total d'établissements
    $requete5=' SELECT count(*) FROM etablissement';
    $resultats=$bdd->query($requete5);
    $compteuretab=$resultats->fetch(PDO::FETCH_ASSOC);
    $resultats->closeCursor();
?>

<?php if (isset($_GET['id']) && $_GET['id'] > $compteuretab['count(*)'])
{
    header('location:404.php');
}

else {
?>



<header id="single">
    <?php include('php/nav.php'); ?>
</header>

<div class="single_barre">
    <div class="barre barre_1"></div>
    <div class="barre barre_2"></div>
</div>

<section class="single">
    <div class="single_galery">
        <?php foreach($images as $key => $image): ?>
            <?php if($key == 0): ?>
                <div class="img_galerie visible"><img src="images/<?= $image->url_image; ?>" alt="<?= $etablissement->nom_etablissement; ?>"></div>
            <?php else: ?>
                <div class="img_galerie"><img src="images/<?= $image->url_image; ?>" alt="<?= $etablissement->nom_etablissement; ?>"></div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>

    <div class="single_container">
        <div class="single_top">
            <p class="filariane">
                <a href="index.php">Accueil</a>
                <span class="slash"> / </span>
                <a href="list.php">Membres</a>
                <span class="slash"> / </span>
                <span><?= $etablissement->nom_etablissement; ?></span>
            </p>

            <p class="single_surtitre"><?= $etablissement->departement_etablissement; ?></p>
            <h1><?= $etablissement->nom_etablissement; ?></h1>

            <div class="single_spe">

                <p class="single_located">
                    <i class="fas fa-map-marker-alt"></i>
                    <span><?= $etablissement->lieu_etablissement; ?></span>
                    <span class="single_slash"> | </span>
                    <i class="fas fa-tags"></i>
                    <span class="single_tags">
                        <?php foreach($specialites as $key => $specialite): ?>
                            <?php if($key != 0): ?>
                                <span> - </span>
                            <?php endif; ?>
                            <a href="list.php?filtre=<?= strtolower($specialite->abreviation_specialite); ?>"><?= $specialite->abreviation_specialite; ?></a>
                        <?php endforeach; ?>
                    </span>
                </p>
            </div>
            <p class="single_exerpt">
                <?= $etablissement->extrait_etablissement; ?>
            </p>
            <a class="single_ensavoirplus" href="#single_bot">En savoir plus</a>
        </div>

        <div class="single_bot" id="single_bot">

            <p>
                <?= $etablissement->texte_etablissement; ?>
            </p>

        </div>
    </div>

    <div class="single_img" >
        <img src="images/<?= $etablissement->image_etablissement; ?>" alt="<?= $etablissement->nom_etablissement; ?>">
    </div>
</section>


<?php include('php/footer.php'); ?>


<?php
};
?>
